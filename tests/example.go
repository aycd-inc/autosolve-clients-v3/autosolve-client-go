package main

import (
	"bufio"
	"gitlab.com/aycd-inc/autosolve-clients-v3/autosolve-client-go/autosolve"
	"log"
	"os"
	"strings"
	"time"
)

type listenerImpl struct {
	autosolve.Listener

	service *AutoSolveService
}

func (l *listenerImpl) OnStatusChanged(status autosolve.Status) {
	log.Printf("Status changed: %v\n", status)
}

func (l *listenerImpl) OnError(err error) {
	log.Printf("Error: %v\n", err)
}

func (l *listenerImpl) OnTokenResponse(tokenResponse *autosolve.CaptchaTokenResponse) {
	log.Printf("Token response: %v\n", tokenResponse)
	solveResponse := &CaptchaSolveResponse{
		Cancelled: false,
		Response:  tokenResponse,
	}
	respChan := l.service.responseMap[tokenResponse.TaskId]
	if respChan != nil {
		respChan <- solveResponse
	}
}

func (l *listenerImpl) OnTokenCancelResponse(cancelResponse *autosolve.CaptchaTokenCancelResponse) {
	log.Printf("Token cancel response: %v\n", cancelResponse)
	solveResponse := &CaptchaSolveResponse{
		Cancelled: true,
		Response:  cancelResponse,
	}
	for _, request := range cancelResponse.Requests {
		respChan := l.service.responseMap[request.TaskId]
		if respChan != nil {
			respChan <- solveResponse
		}
	}
}

type CaptchaSolveResponse struct {
	Cancelled bool
	Timeout   bool
	Response  autosolve.CaptchaResponse
}

func (r *CaptchaSolveResponse) SolveResponse() *autosolve.CaptchaTokenResponse {
	return r.Response.(*autosolve.CaptchaTokenResponse)
}

func (r *CaptchaSolveResponse) CancelResponse() *autosolve.CaptchaTokenCancelResponse {
	return r.Response.(*autosolve.CaptchaTokenCancelResponse)
}

type AutoSolveService struct {
	listener    *listenerImpl
	session     autosolve.Session
	responseMap map[string]chan *CaptchaSolveResponse
}

func NewAutoSolveService(clientId string) *AutoSolveService {
	autosolve.Init(clientId)
	service := &AutoSolveService{
		responseMap: make(map[string]chan *CaptchaSolveResponse),
	}
	service.listener = &listenerImpl{
		service: service,
	}
	return service
}

func (s *AutoSolveService) Connect(apiKey string) error {
	session, err := autosolve.GetSession(apiKey)
	if err != nil {
		return err
	}
	s.session = session
	s.session.SetListener(s.listener)
	return s.session.Open()
}

func (s *AutoSolveService) Close() error {
	if s.session != nil {
		return s.session.Close()
	}
	return nil
}

func (s *AutoSolveService) Solve(tokenRequest *autosolve.CaptchaTokenRequest) (*CaptchaSolveResponse, error) {
	if s.session == nil {
		return nil, autosolve.InvalidSessionError
	}
	channel := make(chan *CaptchaSolveResponse)
	s.responseMap[tokenRequest.TaskId] = channel
	s.session.Send(tokenRequest)
	return <-channel, nil
}

func (s *AutoSolveService) SolveWithTimeout(tokenRequest *autosolve.CaptchaTokenRequest, timeout time.Duration) (*CaptchaSolveResponse, error) {
	if s.session == nil {
		return nil, autosolve.InvalidSessionError
	}
	channel := make(chan *CaptchaSolveResponse)
	s.responseMap[tokenRequest.TaskId] = channel
	s.session.Send(tokenRequest)
	select {
	case msg := <-channel:
		return msg, nil
	case <-time.After(timeout):
		return &CaptchaSolveResponse{
			Cancelled: false,
			Timeout:   true,
			Response:  nil,
		}, nil
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	clientId := os.Getenv("CLIENT_ID")
	if len(clientId) == 0 {
		_, _ = os.Stdout.WriteString("ClientID: ")
		clientId, _ = reader.ReadString('\n')
		clientId = strings.TrimSpace(clientId)
	}
	apiKey := os.Getenv("API_KEY")
	if len(apiKey) == 0 {
		_, _ = os.Stdout.WriteString("ApiKey: ")
		apiKey, _ = reader.ReadString('\n')
		apiKey = strings.TrimSpace(apiKey)
	}
	service := NewAutoSolveService(clientId)
	err := service.Connect(apiKey)
	if err != nil {
		log.Fatal(err)
	}
	var message = &autosolve.CaptchaTokenRequest{
		TaskId:  "test1",
		Url:     "https://recaptcha.autosolve.io/version/1",
		SiteKey: "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b",
		Version: autosolve.ReCaptchaV2Checkbox,
	}
	solveResponse, err := service.Solve(message)
	if err != nil {
		log.Fatal(err)
	}
	if solveResponse.Cancelled {
		log.Printf("Cancelled")
	} else if solveResponse.Timeout {
		log.Printf("Timed Out")
	} else {
		log.Printf("Token: %v", solveResponse.SolveResponse().Token)
	}
	message = &autosolve.CaptchaTokenRequest{
		TaskId:  "test2",
		Url:     "https://recaptcha.autosolve.io/version/3",
		SiteKey: "6LfPLMAUAAAAAOxrrII-JjeTjGsMN9cb_45sqEWK",
		Action:  "examples/v3scores",
		Version: autosolve.ReCaptchaV3,
	}
	solveResponse, err = service.SolveWithTimeout(message, 30*time.Second)
	if solveResponse.Cancelled {
		log.Printf("Cancelled")
	} else if solveResponse.Timeout {
		log.Printf("Timed Out")
	} else {
		log.Printf("Token: %v", solveResponse.SolveResponse().Token)
	}
	err = service.Close()
	if err != nil {
		log.Fatal(err)
	}
}

package main

import (
	"gitlab.com/aycd-inc/autosolve-clients-v3/autosolve-client-go/autosolve"
	"log"
	"os"
	"sync"
	"testing"
)

var (
	wg = sync.WaitGroup{}
)

type ListenerImpl struct {
	autosolve.Listener
}

func (l *ListenerImpl) OnStatusChanged(status autosolve.Status) {
	log.Printf("Status changed: %v\n", status)
}

func (l *ListenerImpl) OnError(err error) {
	log.Printf("Error: %v\n", err)
}

func (l *ListenerImpl) OnTokenResponse(tokenResponse *autosolve.CaptchaTokenResponse) {
	log.Printf("Token response: %v\n", tokenResponse)
	wg.Done()
}

func (l *ListenerImpl) OnTokenCancelResponse(cancelResponse *autosolve.CaptchaTokenCancelResponse) {
	log.Printf("Token cancel response: %v\n", cancelResponse)
	wg.Done()
}

func TestSend(t *testing.T) {
	clientId := os.Getenv("CLIENT_ID")
	apiKey := os.Getenv("API_KEY")
	var listener = &ListenerImpl{}
	autosolve.Init(clientId)
	session, err := autosolve.GetSession(apiKey)
	if err != nil {
		log.Fatal(err)
	}
	session.SetListener(listener)
	err = session.Open()
	if err != nil {
		log.Fatal(err)
	}
	var message = &autosolve.CaptchaTokenRequest{
		TaskId:  "1",
		Url:     "https://recaptcha.autosolve.io/version/1",
		SiteKey: "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b",
	}
	session.Send(message)
	wg.Add(1)
	wg.Wait()
}

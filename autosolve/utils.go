package autosolve

import (
	"strings"
	"time"
)

func replaceAllDashes(key string) string {
	return strings.Replace(key, "-", "", -1)
}

func createKeyWithAccessToken(prefix string, aToken string) string {
	return prefix + "." + aToken
}

func createKeyWithAccountId(prefix string, aId string) string {
	return prefix + "." + aId
}

func createKeyWithAccountIdAndApiKey(prefix string, aId string, apiKey string) string {
	return prefix + "." + aId + "." + apiKey
}

func getCurrentUnixTime() int64 {
	return time.Now().Unix()
}

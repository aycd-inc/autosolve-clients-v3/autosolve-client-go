package autosolve

import (
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"io/ioutil"
	"net/http"
	"time"
)

var (
	reconnectTimeouts    = [7]time.Duration{2, 3, 5, 8, 13, 21, 34}
	connectMinDelay      = 5 * time.Second
	maxReconnectAttempts = 100
)

type Session interface {
	Open() OpenError
	Close() error
	Send(r *CaptchaTokenRequest)
	CancelOne(taskId string)
	CancelMany(taskIds ...string)
	CancelAll()
	SetListener(l Listener)
	GetStatus() Status
}

type Listener interface {
	OnStatusChanged(status Status)
	OnError(err error)
	OnTokenResponse(response *CaptchaTokenResponse)
	OnTokenCancelResponse(response *CaptchaTokenCancelResponse)
}

type emptyListener struct {
	Listener
}

func (l *emptyListener) OnStatusChanged(status Status) {
}

func (l *emptyListener) OnError(err error) {
}

func (l *emptyListener) OnTokenResponse(tokenResponse *CaptchaTokenResponse) {
}

func (l *emptyListener) OnTokenCancelResponse(cancelResponse *CaptchaTokenCancelResponse) {
}

type amqpConfig struct {
	username                    string
	password                    string
	directExchangeName          string
	fanoutExchangeName          string
	responseQueueName           string
	responseTokenRouteKey       string
	responseTokenCancelRouteKey string
	requestTokenRouteKey        string
	requestTokenCancelRouteKey  string
}

type session struct {
	Session
	apiKey             string
	conn               *amqp.Connection
	directChannel      *amqp.Channel
	fanoutChannel      *amqp.Channel
	reconnectAttempt   int
	lastConnectAttempt time.Time
	connecting         *atomicBool
	explicitShutdown   bool
	listener           Listener
	status             Status
	amqpCfg            *amqpConfig
	sendTokenReqChan   chan *CaptchaTokenRequest
	sendCancelChan     chan *CaptchaTokenCancelRequest
	forceReconnect     chan error
	closeChans         []chan *amqp.Error
}

func newSession(apiKey string) *session {
	return &session{
		apiKey:           apiKey,
		connecting:       NewAtomicBool(false),
		sendTokenReqChan: make(chan *CaptchaTokenRequest, 5000),
		sendCancelChan:   make(chan *CaptchaTokenCancelRequest, 5000),
		forceReconnect:   make(chan error),
	}
}

func (s *session) Open() OpenError {
	if !s.connecting.CompareAndSwap(false, true) {
		return ConnectionPendingError
	}
	defer s.connecting.Set(false)
	_ = s.Close()
	s.explicitShutdown = false
	s.setStatus(Connecting)
	err := s.open()
	if err != nil {
		s.setStatus(Disconnected)
		return err
	}
	s.setStatus(Connected)
	return nil
}

func (s *session) Close() error {
	var err error = nil
	s.explicitShutdown = true
	if s.conn != nil && !s.conn.IsClosed() {
		err = s.conn.Close()
	}
	return err
}

func (s *session) open() OpenError {
	if !s.isValidSession() {
		return InvalidSessionError
	}
	currentTime := time.Now()
	elapsedTime := connectMinDelay - currentTime.Sub(s.lastConnectAttempt)
	if elapsedTime > 0 {
		time.Sleep(elapsedTime)
	}
	s.lastConnectAttempt = time.Now()
	err := s.authenticate()
	if err != nil {
		return err
	}
	if s.conn != nil && !s.conn.IsClosed() {
		_ = s.conn.Close()
	}
	s.conn, err = amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:5672/%s?heartbeat=10",
		s.amqpCfg.username, s.amqpCfg.password, hostname, vhost))
	if err != nil {
		s.sendError(err)
		return ConnectError
	}
	s.directChannel, err = s.conn.Channel()
	if err != nil {
		s.sendError(err)
		return ConnectError
	}
	s.fanoutChannel, err = s.conn.Channel()
	if err != nil {
		s.sendError(err)
		return ConnectError
	}
	err = s.registerConsumers()
	if err != nil {
		s.sendError(err)
		return ConnectError
	}
	go s.listenNotifyClose()
	return nil
}

func (s *session) listenNotifyClose() {
	s.closeChans = make([]chan *amqp.Error, 0)
	closeCh1 := make(chan *amqp.Error, 1)
	closeCh2 := make(chan *amqp.Error, 1)
	closeCh3 := make(chan *amqp.Error, 1)
	go s.listenSend()
	s.conn.NotifyClose(closeCh1)
	s.directChannel.NotifyClose(closeCh2)
	s.fanoutChannel.NotifyClose(closeCh3)
	select {
	case err := <-closeCh1:
		s.startReconnect(err)
	case err := <-closeCh2:
		s.startReconnect(err)
	case err := <-closeCh3:
		s.startReconnect(err)
	case <-s.forceReconnect:
		s.startReconnect(nil)
	}
}

func (s *session) startReconnect(err *amqp.Error) {
	for _, closeChan := range s.closeChans {
		closeChan <- err
	}
	if err != nil {
		fmtErr := fmt.Errorf("a connection error has occurred: %v", err)
		s.sendError(fmtErr)
		s.reconnect()
	} else {
		s.setStatus(Disconnected)
	}
}

func (s *session) reconnect() {
	connected := false
	s.setStatus(Reconnecting)
	for i := 0; i < maxReconnectAttempts; i++ {
		timeout := s.nextTimeout()
		duration := timeout * time.Second
		time.Sleep(duration)
		err := s.open()
		if err == InvalidSessionError || err == InvalidApiKeyError {
			s.setStatus(Disconnected)
			return
		}
		if !s.isClosed() {
			s.setStatus(Connected)
			connected = true
			break
		}
	}
	if !connected {
		s.sendError(fmt.Errorf("failed to reconnect after %v attempts", maxReconnectAttempts))
		s.setStatus(Disconnected)
	}
	s.reconnectAttempt = 0
}

func (s *session) nextTimeout() time.Duration {
	var index int
	if s.reconnectAttempt >= len(reconnectTimeouts) {
		index = len(reconnectTimeouts) - 1
	} else {
		index = s.reconnectAttempt
		s.reconnectAttempt += 1
	}
	return reconnectTimeouts[index]
}

func (s *session) isClosed() bool {
	return (s.conn == nil || s.conn.IsClosed()) ||
		(s.directChannel == nil || s.directChannel.IsClosed()) ||
		(s.fanoutChannel == nil || s.fanoutChannel.IsClosed())
}

func (s *session) authenticate() OpenError {
	url := fmt.Sprintf("https://autosolve-dashboard-api.aycd.io/api/v1/auth/verify?clientId=%s&apiKey=%s", clientId, s.apiKey)
	res, err := http.Get(url)
	if err != nil {
		s.sendError(err)
		return ServerError
	}
	if res.StatusCode != 200 {
		if res.StatusCode == 401 {
			return InvalidApiKeyError
		} else {
			return ServerError
		}
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		s.sendError(err)
		return ServerError
	}
	if len(body) > 0 {
		var creds *credentials
		err = json.Unmarshal(body, &creds)
		if err != nil {
			s.sendError(err)
			return ServerError
		}
		fmtPassword := replaceAllDashes(creds.Password)
		fmtApiKey := replaceAllDashes(s.apiKey)
		config := &amqpConfig{
			username: creds.Username,
			password: creds.Password,
			directExchangeName: createKeyWithAccountId(
				directExchangePrefix, creds.Username),
			fanoutExchangeName: createKeyWithAccountId(
				fanoutExchangePrefix, creds.Username),
			responseQueueName: createKeyWithAccountIdAndApiKey(
				responseQueuePrefix, creds.Username, fmtApiKey),
			responseTokenRouteKey: createKeyWithAccountIdAndApiKey(
				responseTokenRoutePrefix, creds.Username, fmtApiKey),
			responseTokenCancelRouteKey: createKeyWithAccountIdAndApiKey(
				responseTokenCancelRoutePrefix, creds.Username, fmtApiKey),
			requestTokenRouteKey: createKeyWithAccessToken(
				requestTokenRoutePrefix, fmtPassword),
			requestTokenCancelRouteKey: createKeyWithAccessToken(
				requestTokenCancelRoutePrefix, fmtPassword),
		}
		s.amqpCfg = config
		return nil
	}
	return ServerError
}

func (s *session) setStatus(status Status) {
	if s.status != status {
		s.status = status
		s.listener.OnStatusChanged(status)
	}
}

func (s *session) sendError(err error) {
	s.listener.OnError(err)
}

func (s *session) Send(request *CaptchaTokenRequest) {
	request.ApiKey = s.apiKey
	request.CreatedAt = getCurrentUnixTime()
	s.sendTokenReqChan <- request
}

func (s *session) CancelOne(taskId string) {
	s.CancelMany([]string{taskId}...)
}

func (s *session) CancelMany(taskIds ...string) {
	if len(taskIds) == 0 {
		return
	}
	request := &CaptchaTokenCancelRequest{
		ApiKey:           s.apiKey,
		CreatedAt:        getCurrentUnixTime(),
		TaskIds:          taskIds,
		ResponseRequired: true,
	}
	s.sendCancelChan <- request
}

func (s *session) CancelAll() {
	var request = &CaptchaTokenCancelRequest{
		ApiKey:           s.apiKey,
		CreatedAt:        getCurrentUnixTime(),
		ResponseRequired: false,
	}
	s.sendCancelChan <- request
}

func (s *session) listenSend() {
	closeChan := s.addCloseChan()
	for {
		select {
		case <-closeChan:
			return
		case tokenRequest := <-s.sendTokenReqChan:
			var jsonData []byte
			jsonData, err := json.Marshal(tokenRequest)
			if err != nil {
				s.sendError(fmt.Errorf("failed to marshal CaptchaTokenRequest with error: %v", err))
			}
			err = s.directChannel.Publish(
				s.amqpCfg.directExchangeName,
				s.amqpCfg.requestTokenRouteKey,
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        jsonData,
				})
			if err != nil {
				s.sendTokenReqChan <- tokenRequest
				s.sendError(fmt.Errorf("failed to send token request with error: %v", err))
				return
			}
		case cancelRequest := <-s.sendCancelChan:
			var jsonData []byte
			jsonData, err := json.Marshal(cancelRequest)
			if err != nil {
				s.sendError(fmt.Errorf("failed to marshal CaptchaTokenRequest with error: %v", err))
			}
			err = s.directChannel.Publish(
				s.amqpCfg.fanoutExchangeName,
				s.amqpCfg.requestTokenCancelRouteKey,
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        jsonData,
				})
			if err != nil {
				s.sendCancelChan <- cancelRequest
				s.sendError(fmt.Errorf("failed to send captcha request"))
				return
			}
		}
	}
}

func (s *session) SetListener(listener Listener) {
	if listener == nil {
		s.listener = &emptyListener{}
	} else {
		s.listener = listener
	}
}

func (s *session) GetStatus() Status {
	return s.status
}

func (s *session) addCloseChan() chan *amqp.Error {
	closeCh := make(chan *amqp.Error)
	s.closeChans = append(s.closeChans, closeCh)
	return closeCh
}

func (s *session) registerConsumers() error {
	err := s.bindQueues()
	if err != nil {
		return err
	}
	messages, err := s.directChannel.Consume(
		s.amqpCfg.responseQueueName,
		"",
		autoAckQueue,
		exclusiveQueue,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	go func() {
		for delivery := range messages {
			switch delivery.RoutingKey {
			case s.amqpCfg.responseTokenRouteKey:
				s.processTokenMessage(&delivery)
			case s.amqpCfg.responseTokenCancelRouteKey:
				s.processTokenCancelMessage(&delivery)
			}
		}
	}()
	return nil
}

func (s *session) processTokenMessage(message *amqp.Delivery) {
	var tokenResponse CaptchaTokenResponse
	var err = json.Unmarshal(message.Body, &tokenResponse)
	if err != nil {
		s.sendError(err)
	} else {
		s.listener.OnTokenResponse(&tokenResponse)
	}
}

func (s *session) processTokenCancelMessage(message *amqp.Delivery) {
	var tokenCancelResponse CaptchaTokenCancelResponse
	var err = json.Unmarshal(message.Body, &tokenCancelResponse)
	if err != nil {
		s.sendError(err)
	} else {
		s.listener.OnTokenCancelResponse(&tokenCancelResponse)
	}
}

func (s *session) bindQueues() error {
	var err error
	err = s.directChannel.QueueBind(
		s.amqpCfg.responseQueueName,
		s.amqpCfg.responseTokenRouteKey,
		s.amqpCfg.directExchangeName,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	return s.directChannel.QueueBind(
		s.amqpCfg.responseQueueName,
		s.amqpCfg.responseTokenCancelRouteKey,
		s.amqpCfg.directExchangeName,
		false,
		nil,
	)
}

func (s *session) isValidSession() bool {
	return hasSession(s)
}

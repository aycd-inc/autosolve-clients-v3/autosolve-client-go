package autosolve

import "sync/atomic"

type atomicBool struct{ flag int32 }

func NewAtomicBool(b bool) *atomicBool {
	aBool := &atomicBool{flag: ConvertBoolToInt32(b)}
	return aBool
}

func (b *atomicBool) Set(value bool) {
	atomic.StoreInt32(&(b.flag), ConvertBoolToInt32(value))
}

func (b *atomicBool) CompareAndSwap(old bool, new bool) bool {
	return atomic.CompareAndSwapInt32(&(b.flag), ConvertBoolToInt32(old), ConvertBoolToInt32(new))
}

func (b *atomicBool) Get() bool {
	if atomic.LoadInt32(&(b.flag)) != 0 {
		return true
	}
	return false
}

func ConvertBoolToInt32(b bool) int32 {
	var i int32 = 0
	if b {
		i = 1
	}
	return i
}

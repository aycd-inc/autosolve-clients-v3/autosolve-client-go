package autosolve

import (
	"errors"
	"strings"
	"sync"
)

const (
	hostname             = "amqp.autosolve.aycd.io"
	vhost                = "oneclick"
	directExchangePrefix = "exchanges.direct"
	fanoutExchangePrefix = "exchanges.fanout"

	responseQueuePrefix = "queues.response.direct"

	requestTokenRoutePrefix        = "routes.request.token"
	requestTokenCancelRoutePrefix  = "routes.request.token.cancel"
	responseTokenRoutePrefix       = "routes.response.token"
	responseTokenCancelRoutePrefix = "routes.response.token.cancel"

	autoAckQueue   = true
	exclusiveQueue = false
)

var (
	once                sync.Once
	sessionLock         sync.Mutex
	clientId            string
	sessions            map[string]Session
	multiSessionEnabled bool
)

func Init(cId string) {
	cId = strings.TrimSpace(cId)
	if len(cId) == 0 {
		return
	}
	once.Do(func() {
		clientId = cId
		sessions = make(map[string]Session)
	})
}

func GetSession(apiKey string) (Session, error) {
	if len(clientId) == 0 {
		return nil, errors.New("autosolve client has not been initialized")
	}
	apiKey = strings.TrimSpace(apiKey)
	if len(apiKey) == 0 {
		return nil, errors.New("apiKey cannot be empty")
	}
	sessionLock.Lock()
	defer sessionLock.Unlock()
	session := sessions[apiKey]
	if session != nil {
		return session, nil
	}
	session = newSession(apiKey)
	if !multiSessionEnabled {
		for _, s := range sessions {
			_ = s.Close()
		}
		sessions = make(map[string]Session)
	}
	sessions[apiKey] = session
	return session, nil
}

func RemoveSession(apiKey string) {
	if !multiSessionEnabled {
		return
	}
	apiKey = strings.TrimSpace(apiKey)
	if len(apiKey) == 0 {
		return
	}
	sessionLock.Lock()
	defer sessionLock.Unlock()
	delete(sessions, apiKey)
}

func hasSession(s *session) bool {
	sessionLock.Lock()
	defer sessionLock.Unlock()
	return sessions[s.apiKey] == s
}

func SetMultiSessionEnabled(enabled bool) {
	multiSessionEnabled = enabled
}

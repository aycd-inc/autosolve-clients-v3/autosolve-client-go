package autosolve

import "errors"

type CaptchaTokenRequest struct {
	TaskId           string            `json:"taskId"`
	ApiKey           string            `json:"apiKey"`
	CreatedAt        int64             `json:"createdAt"`
	Url              string            `json:"url"`
	SiteKey          string            `json:"siteKey"`
	Version          int               `json:"version"`
	Action           string            `json:"action"`
	MinScore         float32           `json:"minScore"`
	Proxy            string            `json:"proxy"`
	ProxyRequired    bool              `json:"proxyRequired"`
	UserAgent        string            `json:"userAgent"`
	Cookies          string            `json:"cookies"`
	RenderParameters map[string]string `json:"renderParameters"`
	Metadata         map[string]string `json:"metadata"`
}

type CaptchaTokenCancelRequest struct {
	TaskId           string   `json:"taskId"`
	ApiKey           string   `json:"apiKey"`
	CreatedAt        int64    `json:"createdAt"`
	TaskIds          []string `json:"taskIds"`
	ResponseRequired bool     `json:"responseRequired"`
}

type CaptchaResponse interface {
}

type CaptchaTokenResponse struct {
	CaptchaResponse
	TaskId    string              `json:"taskId"`
	ApiKey    string              `json:"apiKey"`
	CreatedAt int64               `json:"createdAt"`
	Request   CaptchaTokenRequest `json:"request"`
	Token     string              `json:"token"`
}

type CaptchaTokenCancelResponse struct {
	CaptchaResponse
	Requests []CaptchaTokenRequest `json:"requests"`
}

type Status string

const (
	Connecting   Status = "Connecting"
	Connected    Status = "Connected"
	Reconnecting Status = "Reconnecting"
	Disconnected Status = "Disconnected"
)

const (
	ReCaptchaV2Checkbox   int = 0
	ReCaptchaV2Invisible  int = 1
	ReCaptchaV3           int = 2
	GeeTest               int = 5
	ReCaptchaV3Enterprise int = 6
	ReCaptchaV2Enterprise int = 7
	FunCaptcha            int = 8
)

type OpenError error

var (
	InvalidApiKeyError     = errors.New("the apiKey for this session is not valid")
	InvalidSessionError    = errors.New("the session is no longer valid")
	ConnectError           = errors.New("failed to connect to autosolve")
	ConnectionPendingError = errors.New("the session is already attempting to open a connection")
	ServerError            = errors.New("failed to reach auth server")
)

type credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
